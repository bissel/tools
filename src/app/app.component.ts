import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {

  flag = '🏳' + 'U+200D' + '🌈'
  title = 'tools.bissel.it' + this.flag;
}
