import {Routes} from "@angular/router";
import {ContrastComponent} from "./tools/contrast/contrast.component";
import {QrCodeComponent} from "./tools/qr-code/qr-code.component";


export const routes: Routes = [
  {
    path: 'contrast',
    component: ContrastComponent
  },
  {
    path: 'qr-code',
    component: QrCodeComponent
  },
  {
    path: '',
    redirectTo: '/contrast',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '/404'
  }
]
