import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {startWith} from "rxjs";
import {QRCodeModule} from "angularx-qrcode";
import {NgClass, NgIf, NgStyle} from "@angular/common";
import {MatSelectModule} from "@angular/material/select";
import {MatInputModule} from "@angular/material/input";

enum WifiType {
  WPA,
  WEP,
}
enum QRCodeType {
  Normal,
  Wifi
}

interface QRCodeForm {
  type: QRCodeType,
  wifi: {
    ssid: string,
    password: string,
    type: WifiType
  },
  normal: {
    text: string,
  }
}

@Component({
  selector: 'app-qr-code',
  templateUrl: './qr-code.component.html',
  styleUrls: [
    '../base.css',
    './qr-code.component.css'
  ],
  standalone: true,
  imports: [
    NgClass,
    NgStyle,
    NgIf,

    ReactiveFormsModule,
    QRCodeModule,
    MatSelectModule,
    MatInputModule,
    FormsModule
  ]
})
export class QrCodeComponent implements OnInit {
  protected QRCodeType = QRCodeType;
  protected WifiType = WifiType;
  protected qrCodeData: null | string = null;

  private static generateWifiString(ssid: string, password: string, type: WifiType) {
    return `WIFI:S:${ssid};T:${type ? WifiType[type] : ''};P:${password};;`;
  }

  protected form = new FormGroup({
    type: new FormControl<QRCodeType>(QRCodeType.Normal, {nonNullable: true}),
    wifi: new FormGroup({
      ssid: new FormControl<string>('', {nonNullable: true}),
      password: new FormControl<string>('', {nonNullable: true}),
      type: new FormControl<WifiType>(WifiType.WPA, {nonNullable: true})
    }),
    normal: new FormGroup({
      text: new FormControl<string>('', {nonNullable: true})
    })
  })

  ngOnInit(): void {
    this.form.valueChanges
      .pipe(
        startWith(this.form.getRawValue())
      )
      .subscribe( (value) => {
        this.setQrCodeData();
      })
  }

  private setQrCodeData() {

    this.qrCodeData = this.form.controls.type.value === QRCodeType.Normal
      ? this.form.controls.normal.controls.text.value
      : QrCodeComponent.generateWifiString(
        this.form.controls.wifi.controls.ssid.value,
        this.form.controls.wifi.controls.password.value,
        this.form.controls.wifi.controls.type.value,
      )

  }
}
