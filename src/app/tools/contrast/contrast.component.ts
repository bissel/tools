import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators} from "@angular/forms";
import {DecimalPipe, NgClass, NgStyle} from "@angular/common";
import {filter, map, Observable, Subject} from "rxjs";

function numberToHex(n: number) {
  return n.toString(16).padStart(2, '0');
}

function lerp(a: number, b: number, l: number) {
  return (a * (1 - l)) + (b * l);
}

class ColorRGB {

  constructor(public r: number, public g: number, public b: number) {}

  public static fromHex(hex: string): ColorRGB {
    hex = hex.length === 3 ? `${hex[0]}${hex[0]}${hex[1]}${hex[1]}${hex[2]}${hex[2]}` : hex;
    return new ColorRGB(
      parseInt(hex[0] + hex[1], 16),
      parseInt(hex[2] + hex[3], 16),
      parseInt(hex[4] + hex[5], 16),
    );
  }

  public getRelativeLuminance() {
    const R = this.getRelativeLuminanceChanel(this.r);
    const G = this.getRelativeLuminanceChanel(this.g);
    const B = this.getRelativeLuminanceChanel(this.b);

    return 0.2126 * R + 0.7152 * G + 0.0722 * B;
  }

  private getRelativeLuminanceChanel(value: number) {
    const sRGB = value / 255;
    return sRGB <= 0.04045 ? sRGB/12.92 : Math.pow((sRGB+0.055)/1.055, 2.4)
  }

  public asHex() {
    return `#${numberToHex(this.r)}${numberToHex(this.g)}${numberToHex(this.b)}`
  }

  public toString(): string {
    return `rgb(${this.r},${this.g},${this.b})`;
  }
}

class ColorRGBA extends ColorRGB {
  constructor(r: number, g: number, b: number, public a: number = 1) {
    super(r,g,b);
  }

  public static fromHexAlpha(hex: string, alpha: number): ColorRGBA {
    const rgb = ColorRGB.fromHex(hex);
    return new ColorRGBA(rgb.r, rgb.g, rgb.b, alpha);
  }

  public asRGB(background: ColorRGB) : ColorRGB {
    const l = 1 - this.a;
    return new ColorRGB(
      lerp(this.r, background.r, l),
      lerp(this.g, background.g, l),
      lerp(this.b, background.b, l)
    );
  }

  public override toString(): string {
    return `rgba(${this.r},${this.g},${this.b},${this.a})`;
  }
}

@Component({
  selector: 'app-contrast',
  templateUrl: './contrast.component.html',
  styleUrls: [
    '../base.css',
    './contrast.component.css'
  ],
  standalone: true,
  imports: [
    NgClass,
    NgStyle,

    DecimalPipe,

    FormsModule,
    ReactiveFormsModule
  ]
})
export class ContrastComponent implements OnInit {

  protected static validatorsRGBComponent = [Validators.required, Validators.min(0), Validators.max(255)];
  protected static validatorsAlphaComponent = [Validators.required, Validators.min(0), Validators.max(1)];

  private foregroundColor = new ColorRGBA(255, 255, 255, 1);
  private backgroundColor = new ColorRGBA(0, 0, 0, 1);

  private foregroundColor$= new Subject<ColorRGBA>();
  private backgroundColor$= new Subject<ColorRGBA>();

  protected form = new FormGroup({
    foregroundColorHex: new FormControl<string>(this.foregroundColor.asHex()),
    backgroundColorHex: new FormControl<string>(this.backgroundColor.asHex()),
    foregroundColor: new FormGroup({
      r: new FormControl<number>(this.foregroundColor.r, ContrastComponent.validatorsRGBComponent),
      g: new FormControl<number>(this.foregroundColor.g, ContrastComponent.validatorsRGBComponent),
      b: new FormControl<number>(this.foregroundColor.b, ContrastComponent.validatorsRGBComponent),
      a: new FormControl<number>(this.foregroundColor.a, {validators: ContrastComponent.validatorsAlphaComponent}),
    }),
    backgroundColor: new FormGroup({
      r: new FormControl<number>(this.backgroundColor.r, ContrastComponent.validatorsRGBComponent),
      g: new FormControl<number>(this.backgroundColor.g, ContrastComponent.validatorsRGBComponent),
      b: new FormControl<number>(this.backgroundColor.b, ContrastComponent.validatorsRGBComponent),
      a: new FormControl<number>(this.backgroundColor.a, ContrastComponent.validatorsAlphaComponent),
    }),
  })

  protected get foregroundColorWithAlpha () {
    return this.form.controls.foregroundColor.valid ? this.foregroundColor.toString() : '#000';
  }
  protected get foregroundColorHex () { return (this.form.controls.foregroundColorHex.valid ? this.form.controls.foregroundColorHex.value : undefined) ?? '#000' };
  protected get backgroundColorHex () { return (this.form.controls.backgroundColorHex.valid ? this.form.controls.backgroundColorHex.value : undefined) ?? '#fff' };

  protected contrast: number = 0;

  ngOnInit(): void {
    this.form.controls.foregroundColor.valueChanges.subscribe( ({r,g,b,a}) => {
      if(r === null || g === null || b === null || a === null || r === undefined || g === undefined || b === undefined || a === undefined )
        return;

      this.foregroundColor$.next(new ColorRGBA(r | 0, g | 0, b | 0, a));
    })
    this.form.controls.backgroundColor.valueChanges.subscribe( ({r,g,b,a}) => {
      if(r === null || g === null || b === null || a === null || r === undefined || g === undefined || b === undefined || a === undefined )
        return;

      this.backgroundColor$.next(new ColorRGBA(r | 0, g | 0, b | 0, 1))
    })

    this.form.controls.foregroundColorHex.valueChanges
      .pipe(this.hexValueChange)
      .subscribe( (hex) => {
      this.foregroundColor$.next(ColorRGBA.fromHexAlpha(hex, this.foregroundColor.a))
    })
    this.form.controls.backgroundColorHex.valueChanges
      .pipe(this.hexValueChange)
      .subscribe( (hex) => {
      this.backgroundColor$.next(ColorRGBA.fromHexAlpha(hex, this.backgroundColor.a))
    })

    this.foregroundColor$.subscribe((color) => {
      this.foregroundColor = color;
      this.form.controls.foregroundColorHex.setValue(color.asHex(), { emitEvent: false })
      this.form.controls.foregroundColor.controls.r.setValue(color.r, { emitEvent: false })
      this.form.controls.foregroundColor.controls.g.setValue(color.g, { emitEvent: false })
      this.form.controls.foregroundColor.controls.b.setValue(color.b, { emitEvent: false })
      this.form.controls.foregroundColor.controls.a.setValue(color.a, { emitEvent: false })

      this.calcContrast(this.foregroundColor, this.backgroundColor)
    } )
    this.backgroundColor$.subscribe((color) => {
      this.backgroundColor = color;
      this.form.controls.backgroundColorHex.setValue(color.asHex(), { emitEvent: false })
      this.form.controls.backgroundColor.controls.r.setValue(color.r, { emitEvent: false })
      this.form.controls.backgroundColor.controls.g.setValue(color.g, { emitEvent: false })
      this.form.controls.backgroundColor.controls.b.setValue(color.b, { emitEvent: false })
      this.form.controls.backgroundColor.controls.a.setValue(color.a, { emitEvent: false })

      this.calcContrast(this.foregroundColor, this.backgroundColor)
    } )

    this.calcContrast(this.foregroundColor, this.backgroundColor)

    // combineLatest([this.foregroundColor$, this.backgroundColor$])
    //   .pipe(startWith([this.foregroundColor, this.backgroundColor]))
    //   .subscribe( ([f, b]) => {
    //     console.log({f,b})
    //     this.calcContrast(f, b)
    // } )
  }

  private calcContrast(fg: ColorRGBA, bg: ColorRGBA) {
    let L1 = fg.asRGB(bg).getRelativeLuminance();
    let L2 = bg.getRelativeLuminance();

    if(L1 < L2) [L2, L1] = [L1, L2];

    this.contrast = Math.floor(((L1 + 0.05) / (L2 + 0.05)) * 10) / 10;
  }

  private hexValueChange(valueChanged$: Observable<string|null>) {
    return valueChanged$
      .pipe(
        filter(v => !!v),
        map(value => value!.replace('#', '')),
        filter(v => v.length === 3 || v.length === 6),
      )
  }
}
